const reverseArray = (array) => {
    const reverseArray = [];
    const arrayLength = array.length;

    for (let i = 0; i < arrayLength / 2; i++) {
        reverseArray[i] = array[arrayLength-i-1];
        reverseArray[arrayLength-i-1] = array[i];
    }

    console.log('Source - ', array);
    console.log('Return - ', reverseArray);

    return reverseArray;
}


reverseArray(['Apple', 'Banana', 'Orange', 'Coconut']);
reverseArray([1, 2, 3, 4, 5, 6]);