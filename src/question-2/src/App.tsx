import axios from "axios";
import { motion } from "framer-motion";
import { range } from "lodash";
import React, { useEffect, useState } from "react";
import Card from "./components/Card";
import Modal from "./components/Modal";
import ThemeToggle from "./components/ThemeToggle";

const CatFactsAdapter = (result: any) => {
  const catFacts: Array<CatFact> = [];
  const { data } = result;
  (data as []).forEach((factObject: any) => {
    catFacts.push({ fact: factObject.fact });
  });
  return catFacts;
};

const App = () => {
  const [selectedId, setSelectedId] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  const [catFacts, setCatFacts] = useState<Array<CatFact>>([]);

  const resetSelectedCard = () => {
    setSelectedId("");
  };

  useEffect(() => {
    axios
      .get("https://catfact.ninja/facts?limit=9")
      .then((result) => {
        const unsortedCatFacts = CatFactsAdapter(result.data);
        const sortedCatFacts = unsortedCatFacts.sort((a, b) =>
          a.fact[0].localeCompare(b.fact[0])
        );
        //Interval to just to show the loading screen
        setTimeout(() => {
          setCatFacts(sortedCatFacts);
          setIsLoading(false);
        }, 2000);
      })
      .catch((exception) => {
        console.error(
          "An error occured while retrieving the information.",
          exception
        );
      });
  }, []);

  return (
    <div className="p-4 flex items-center justify-start flex-col bg-background dark:bg-darkBackground h-full min-h-screen transition-all">
      <main className="flex items-center justify-center flex-col w-full max-w-screen-xl">
        <div className="flex justify-between items-center w-full">
          <h1 className="my-6 self-start font-bold text-5xl text-black dark:text-gray-200">
            Cat Facts
          </h1>
          <ThemeToggle />
        </div>

        <div className="box-border w-full grid grid-cols-1 lg:grid-cols-3 gap-4 justify-items-center transition-all duration-500">
          {isLoading && (
            <>
              {range(0, 5).map((value, index) => {
                return (
                  <Card key={index} fact="Loading..." isLoading={isLoading} />
                );
              })}
            </>
          )}
          {catFacts.map((catFact, index) => {
            return (
              <Card
                key={index}
                id={`${index}`}
                fact={catFact.fact}
                onClick={() => {
                  setSelectedId(`${index}`);
                }}
                isLoading={isLoading}
              />
            );
          })}

          {selectedId && (
            <Modal onClick={() => resetSelectedCard()}>
              <motion.div
                layout
                key={selectedId}
                className="max-w-md lg:max-w-lg h-64 w-full p-5 cursor-pointer shadow-lg rounded-lg bg-white dark:bg-cardDarkColor text-black z-20"
                layoutId={selectedId}
                onClick={() => resetSelectedCard()}
              >
                <h2 className="text-lg font-bold dark:text-gray-200">
                  Fact {parseInt(selectedId) + 1}
                </h2>
                <motion.p className="text-black dark:text-gray-400 text-base line-clamp-9">
                  {catFacts[parseInt(selectedId)].fact}
                </motion.p>
              </motion.div>
            </Modal>
          )}
        </div>
      </main>
    </div>
  );
};

export default App;
