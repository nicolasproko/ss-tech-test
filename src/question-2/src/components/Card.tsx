import { motion } from "framer-motion";
import clsx from "clsx";

type CardProps = CatFact & {
  id?: string;
  onClick?: Function;
  className?: string;
  isLoading?: boolean;
};

const Card = ({
  fact,
  id = "",
  onClick = () => {},
  className,
  isLoading,
}: CardProps) => {
  if (isLoading) {
    return (
      <div
        className={clsx(
          "max-w-md w-full h-52 space-y-4 p-5 rounded-lg shadow-sm bg-white dark:bg-cardDarkColor text-blackoverflow-hidden",
          className
        )}
      >
        <div className="bg-gray-200 dark:bg-gray-500 h-4 rounded-md w-1/5 animate-pulse"></div>
        <div className="space-y-2 animate-pulse">
          <div className="bg-gray-200 dark:bg-gray-500 h-6 rounded-md w-[70%]"></div>
          <div className="bg-gray-200 dark:bg-gray-500 h-6 rounded-md w-[60%]"></div>
          <div className="bg-gray-200 dark:bg-gray-500 h-6 rounded-md w-[80%]"></div>
        </div>
      </div>
    );
  }

  return (
    <motion.div
      layout
      key={id}
      className={clsx(
        "max-w-md w-full h-52 p-5 cursor-pointer rounded-lg shadow-sm bg-white dark:bg-cardDarkColor text-blackoverflow-hidden",
        className
      )}
      layoutId={id}
      whileHover={{ scale: 1.05 }}
      onClick={() => onClick()}
    >
      <h2 className="text-lg font-bold dark:text-gray-200">
        Fact {parseInt(id) + 1}
      </h2>
      <motion.p className="dark:text-gray-400 text-base line-clamp-6">
        {fact}
      </motion.p>
    </motion.div>
  );
};

export default Card;
