import { EventHandler, ReactNode } from "react";
import { createPortal } from "react-dom";

type ModalProps = { children: ReactNode; onClick: Function };

const Modal = ({ children, onClick }: ModalProps) => {
  const handleEscapePress = (event: KeyboardEvent) => {
    if (event.key === "Escape") {
      onClick();
    }
  };

  document.addEventListener("keydown", handleEscapePress, false);

  return createPortal(
    <div
      className="w-full h-full fixed flex flex-col items-center justify-center bg-black bg-opacity-60 z-10"
      onClick={() => onClick()}
    >
      {children}
    </div>,
    document.getElementById("modal")!
  );
};

export default Modal;
