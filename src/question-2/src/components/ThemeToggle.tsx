import React, { useEffect, useState } from "react";
import { MoonIcon, SunIcon } from "@heroicons/react/solid";
import { motion } from "framer-motion";

const DARK_MODE = "dark";
const LIGHT_MODE = "light";

const setThemeInLocalStorage = (theme: string) => {
  const root = window.document.documentElement;
  const isDark = theme === DARK_MODE;

  root.classList.remove(isDark ? LIGHT_MODE : DARK_MODE);
  root.classList.add(theme);

  localStorage.setItem("color-theme", theme);
};

const ThemeToggle = () => {
  const [theme, setTheme] = useState(DARK_MODE);

  useEffect(() => {
    setThemeInLocalStorage(theme);
  }, [theme]);

  return (
    <motion.div
      whileHover={{ scale: 1.1 }}
      className="rounded-full p-2 bg-white dark:bg-cardDarkColor w-12"
    >
      {theme === DARK_MODE ? (
        <MoonIcon
          color="white"
          onClick={() => setTheme(LIGHT_MODE)}
          className="cursor-pointer"
        />
      ) : (
        <SunIcon
          onClick={() => setTheme(DARK_MODE)}
          className="cursor-pointer"
        />
      )}
    </motion.div>
  );
};

export default ThemeToggle;
