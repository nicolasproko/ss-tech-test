# Cat Facts

## Features

- There is a loading state for the cards.
- The cards can be clicked to be opened to better read the cat fact
- A button to switch between dark mode and light mode is available

## Used libraries

- TailwindCSS
- Framer Motion
- Axios
- HeroIcons
- Lodash
- clsx

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Conventional Commits

This repository uses [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#summary) as a standard

_test:_ indicates any type of test code creation or change.

_feat:_ indicates the development of a new feature to the project.

_refactor:_ used when there is a refactoring of code that has no impact on the system's logic/business rules.

_style:_ used when there are code formatting and style changes that do not alter the system in any way.

_fix:_ used when there is a correction of errors that are generating bugs in the system.

_chore:_ indicates changes to the project that do not affect the system or test files. These are developmental changes.

_docs:_ used when there are changes to the project documentation.

_build:_ used to indicate changes that affect the project's build process or external dependencies.

_perf:_ indicates a change that improved system performance.

_ci:_ used for changes to CI configuration files.

_revert:_ indicates the reversal of a previous commit.
