module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        background: "#F4F4F4",
        darkBackground: "#1F1D2C",
        cardDarkColor: "#262636",
      },
      lineClamp: {
        7: "7",
        9: "9",
      },
    },
  },
  plugins: [require("@tailwindcss/line-clamp")],
};
